#
# Cookbook Name:: google-drive-ocamlfuse
# Recipe:: default
#
# Copyright 2014, Stitchy, LLC
#
# All rights reserved - Do Not Redistribute
#

# Install google-drive-ocamlfuse
case node['platform_family']
when "debian"
  apt_repository "alessandro-strada" do
    uri "http://ppa.launchpad.net/alessandro-strada/ppa/ubuntu"
    distribution node['lsb']['codename']
    components ["main"]
    keyserver "keyserver.ubuntu.com"
    key "F639B041"
  end
  package "google-drive-ocamlfuse"
when "fedora"
  package "ocaml"
  package "ocaml-camlp4-devel"
  package "ocaml-ocamldoc"
  package "automake"
  unless Dir.exists? "/usr/src/opam"
    execute "clone-opam-repository" do
      command "git clone https://github.com/OCamlPro/opam.git /usr/src/opam"
    end
    execute "configure-opam" do
      command "./configure"
      cwd "/usr/src/opam/"
    end
    execute "make-opam" do
      command "make"
      cwd "/usr/src/opam/"
    end
    execute "install-opam" do
      command "make install"
      cwd "/usr/src/opam/"
    end
    execute "init-opam" do
      command "opam init"
    end
    execute "update-opam" do
      command "opam update"
    end
    execute "install-google-drive-ocamlfuse" do
      command "opam install -y google-drive-ocamlfuse"
    end
    link "/usr/bin/google-drive-ocamlfuse" do
      to "/root/.opam/system/bin/google-drive-ocamlfuse"
    end
  end
else
  raise "This cookbook does not support the platform_family: #{platform_family}."
end

# Copy the gdfuse executable to /usr/bin.
cookbook_file "gdfuse" do
  path "/usr/bin/gdfuse"
  mode "755"
  action :create_if_missing
end

# Create the fuse group.
group node["google-drive-ocamlfuse"]["group"] do
  gid node["google-drive-ocamlfuse"]["gid"]
end

# Create the google-drive directory under /mnt.
directory "/mnt/google-drive" do
  owner "root"
  group node["google-drive-ocamlfuse"]["group"]
  mode 00750
  action :create
end

# Mount a drive for each label.
node["google-drive-ocamlfuse"]["labels"].each do |label|
  directory "/mnt/google-drive/#{label}" do
    owner "root"
    group node["google-drive-ocamlfuse"]["group"]
    mode 00750
    action :create
  end
  mount "/mnt/google-drive/#{label}" do
    device "gdfuse##{label}"
    fstype "fuse"
    options "allow_other,gid=#{node["google-drive-ocamlfuse"]["gid"]}"
    action [:mount, :enable]
  end
end
